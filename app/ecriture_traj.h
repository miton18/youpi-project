/* ------------------------------------------------------
   Auteur : Collignon Rémi
   			Morvan Guy-Yann
   	Header Fonction ecritureTraj
   	-----------------------------------------------------*/
   	#ifndef ECRITURETRAJ
   	#define ECRITURETRAJ
   	void ecritureTraj(float* tt1, float* tt2, float* tt3, int* ttr, int np);
   	#endif