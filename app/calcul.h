/* ------------------------------------------------------
   Auteur : Collignon Rémi
   			Morvan Guy-Yann
   	Header Fonction calcul
   	Addition de deux entier
   	-----------------------------------------------------*/
#ifndef CALCUL_H
#define CALCUL_H

	#include <stdio.h>
	int calcul(int a, int b);

#endif
